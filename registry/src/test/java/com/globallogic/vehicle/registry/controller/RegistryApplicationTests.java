package com.globallogic.vehicle.registry.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.globallogic.vehicle.registry.entities.Vehicle;
import com.globallogic.vehicle.registry.repository.RegistryRepository;
import liquibase.pro.packaged.E;
import liquibase.pro.packaged.M;
import liquibase.pro.packaged.V;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RegistryApplicationTests {

    @MockBean
    protected RegistryRepository registryRepository;

    @MockBean
    protected ModelMapper modelMapper;

    @Autowired
    protected RegistryController sut;

    @Test
    public void testGetVehicle() {
        VehicleSO vehicleSO = Mockito.mock(VehicleSO.class);
        Vehicle vehicle = new Vehicle();
        vehicle.setVin("vintest");
        vehicle.setBrand("VW");
        vehicle.setModel("Polo");
        vehicle.setProductionYear(2000);
        Mockito.when(registryRepository.findByVin(vehicle.getVin())).thenReturn(vehicle);
        Mockito.when(modelMapper.map(vehicle, VehicleSO.class)).thenReturn(vehicleSO);

        VehicleSO result = sut.get(vehicle.getVin());

        assertEquals(result, vehicleSO);
    }

    @Test
    public void testDeleteVehicle() {
        String VIN = "1234vin32";
        Vehicle vehicle = Mockito.mock(Vehicle.class);
        Mockito.when(registryRepository.findByVin(VIN)).thenReturn(vehicle);

        sut.delete(VIN);

        verify(registryRepository, times(1)).delete(vehicle);
    }

    @Test
    public void testUpdateVehicleShouldSaveUpdatedVehicle() {
        String VIN = "1234vin32";
        Vehicle vehicle = new Vehicle();
        VehicleSO input = new VehicleSO();
        Mockito.when(registryRepository.findByVin(VIN)).thenReturn(vehicle);

        sut.update(VIN, input);

        verify(registryRepository, times(1)).save(vehicle);
    }

    @Test
    public void testUpdateVehicleShouldUpdateAllProperties() {
        String VIN = "1234vin32";
        Vehicle vehicle = new Vehicle();
        VehicleSO input = new VehicleSO();
        input.setBrand("test brand");
        input.setModel("some model");
        input.setProductionYear(1210);
        input.setVin("newvin123");
        Mockito.when(registryRepository.findByVin(VIN)).thenReturn(vehicle);

        sut.update(VIN, input);

        assertEquals(input.getVin(), vehicle.getVin());
        assertEquals(input.getBrand(), vehicle.getBrand());
        assertEquals(input.getProductionYear(), vehicle.getProductionYear());
        assertEquals(input.getModel(), vehicle.getModel());
    }

    @Test
    public void testUpdateVehicleShouldUpdateNotNullableProperties() {
        String VIN = "1234vin32";
        Integer PRODUCTION_YEAR = 1912;
        Vehicle vehicle = new Vehicle();
        vehicle.setProductionYear(PRODUCTION_YEAR);
        vehicle.setVin(VIN);
        VehicleSO input = new VehicleSO();
        input.setBrand("test brand");
        input.setModel("some model");
        Mockito.when(registryRepository.findByVin(VIN)).thenReturn(vehicle);

        sut.update(VIN, input);

        assertEquals(VIN, vehicle.getVin());
        assertEquals(PRODUCTION_YEAR, vehicle.getProductionYear());
        assertEquals(input.getBrand(), vehicle.getBrand());
        assertEquals(input.getModel(), vehicle.getModel());
    }

    @Test
    public void testGetAllVehicles() {
        Vehicle vehicleA = Mockito.mock(Vehicle.class);
        VehicleSO vehicleSOA = Mockito.mock(VehicleSO.class);
        Vehicle vehicleB = Mockito.mock(Vehicle.class);
        VehicleSO vehicleSOB = Mockito.mock(VehicleSO.class);
        List<Vehicle> vehicles = Arrays.asList(vehicleA, vehicleB);
        Mockito.when(registryRepository.findAll()).thenReturn(vehicles);
        Mockito.when(modelMapper.map(vehicleA, VehicleSO.class)).thenReturn(vehicleSOA);
        Mockito.when(modelMapper.map(vehicleB, VehicleSO.class)).thenReturn(vehicleSOB);

        List<VehicleSO> result = sut.getAll();

        assertArrayEquals(new VehicleSO[] {vehicleSOA, vehicleSOB}, result.toArray());
    }

}