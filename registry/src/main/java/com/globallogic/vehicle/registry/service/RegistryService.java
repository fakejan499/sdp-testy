package com.globallogic.vehicle.registry.service;

import com.globallogic.vehicle.registry.controller.VehicleSO;
import com.globallogic.vehicle.registry.entities.Vehicle;
import com.globallogic.vehicle.registry.exceptions.RegistryResourceNotFound;
import com.globallogic.vehicle.registry.repository.RegistryRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RegistryService {

    @Autowired
    private RegistryRepository registryRepository;

    @Autowired
    protected ModelMapper modelMapper;

    public VehicleSO get(String vin) {
        Vehicle found = registryRepository.findByVin(vin);

        if (found == null) {
            throw new RegistryResourceNotFound("Vehicle with given VIN does not exist.");
        }

        return modelMapper.map(found, VehicleSO.class);
    }

    public VehicleSO create(VehicleSO so) {
        Vehicle vehicle = modelMapper.map(so, Vehicle.class);

        return modelMapper.map(registryRepository.save(vehicle), VehicleSO.class);
    }

    public VehicleSO delete(String vin) {
        Vehicle vehicle = registryRepository.findByVin(vin);
        registryRepository.delete(vehicle);
        return modelMapper.map(vehicle, VehicleSO.class);
    }

    public List<VehicleSO> getAll() {
        return registryRepository.findAll().stream().map(v -> modelMapper.map(v, VehicleSO.class)).collect(Collectors.toList());
    }

    public VehicleSO update(String vin, VehicleSO vehicleSO) {
        Vehicle vehicle = registryRepository.findByVin(vin);

        if (vehicleSO.getBrand() != null) vehicle.setBrand(vehicleSO.getBrand());
        if (vehicleSO.getModel() != null) vehicle.setModel(vehicleSO.getModel());
        if (vehicleSO.getVin() != null) vehicle.setVin(vehicleSO.getVin());
        if (vehicleSO.getProductionYear() != null) vehicle.setProductionYear(vehicleSO.getProductionYear());

        registryRepository.save(vehicle);

        return modelMapper.map(vehicle, VehicleSO.class);
    }


}